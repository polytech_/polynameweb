# polyNameWeb

- Les prénoms par département, par année, et ses corrélations hasardeuses 

## Getting Started

### Pour lancer l'API
```
cd ./api
npm install
npm start
```

### Pour lancer l'APP
```
cd ./app
npm install
npm start
```
http://localhost:4200

## Built With

* [nodejs](https://nodejs.org/)
* [npm](https://www.npmjs.com/)