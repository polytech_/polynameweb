DROP TABLE if exists Nom;
DROP TABLE if exists Departement;
DROP TABLE if exists Crime;

DEFAULT CHARSET=utf8
COLLATE=utf8_unicode_ci;

CREATE TABLE Departement
(
    dptId INT NOT NULL,
    libelle VARCHAR(30),
    PRIMARY KEY (dptId)
);

CREATE TABLE Nom
(
  nom varchar(30) NOT NULL,
  dptId INT,
  annee INT(4),
  sexe boolean,
  quantite INT(4),
  PRIMARY KEY(nom,dptId,annee,sexe),
  FOREIGN KEY(dptId) REFERENCES Departement(dptId)
);

CREATE TABLE Crime(
	idCrime INT auto_increment NOT NULL,
	libelle varchar(100) NOT NULL
)
