var mysql = require('mysql');

var db = mysql.createConnection({
  host: "remotemysql.com",
  database: "2aIDY4R6M5",
  user: "2aIDY4R6M5",
  password: "y84C0dOy4F"
});

db.connect(function(err) {
  if (err) throw err;
  console.log("Connected!");
});

function getName(req, res) {  
  var nom = req.query.nom;
  var dpt = req.query.dptId;
  var annee = req.query.annee;

  if(dpt==undefined && annee==undefined)
    var sql = "SELECT nom, SUM(quantite) as quantite FROM Nom where nom=\""+nom+"\" GROUP BY nom";
  else{
    if(dpt==undefined)
      var sql = "SELECT nom, SUM(quantite) as quantite, annee FROM Nom where nom=\""+nom+"\" AND annee="+annee+" GROUP BY nom, annee"
    else if(annee==undefined)
      var sql = "SELECT nom, SUM(quantite) as quantite, dptId FROM Nom where nom=\""+nom+"\" AND dptId="+dpt+" GROUP BY nom, dptId"
    else
      var sql = "SELECT nom, SUM(quantite) as quantite, dptId, annee FROM Nom where nom=\""+nom+"\" AND dptId="+dpt+" AND annee="+annee+" GROUP BY nom, dptId, annee";
  }

  db.query(sql, function (err, result){
    if (err) throw err;
    res.status(200).json(result);
  });
}

function getNames(req, res) {  
  var sql = "SELECT * FROM Nom";
  var i=0;
  for(elmt in req.query){
    guillemet='';
    if(elmt=="nom")guillemet='\"';

    if(i==0)
    {
      sql += " WHERE "+elmt+"="+guillemet+req.query[elmt]+guillemet;
    }
    else {
      sql += " AND "+elmt+"="+req.query[elmt];
    }
    i++;
  }
  sql+=";";

  db.query(sql, function (err, result){
    if (err) throw err;
    res.status(200).json(result);
  });
}

function getDpts(req, res) {
  var sql = "SELECT * FROM Departement";
  db.query(sql, function (err, result){
    if (err) throw err;
    res.status(200).json(result);
  });
}

function getRandomName(req, res){
  qttMax = 3000;
  var sql = "SELECT nom, SUM(quantite) AS quantite FROM Nom WHERE nom<>\"_PRENOMS_RARES\" GROUP BY nom HAVING quantite>="+qttMax+" ORDER BY rand() LIMIT 100;";
  db.query(sql, function (err, result){
    if (err) throw err;
    res.status(200).json(result);
  });
}

function getRandomCrimes(req, res){
  var sql = "SELECT libele, SUM(quantite) AS quantite FROM Crime, CrimeDpt WHERE Crime.crimeId = CrimeDpt.crimeId GROUP BY libele ORDER BY rand() LIMIT 100;";
  db.query(sql, function (err, result){
    if (err) throw err;
    res.status(200).json(result);
  });
}

function getTop10(req, res) {  
  var dpt = req.query.dptId;
  var annee = req.query.annee;

  var cond_sex = "";
  if(req.query.sexe == "M")
  cond_sex = "sexe = 1 AND ";
  if(req.query.sexe == "F")
  cond_sex = "sexe = 2 AND ";

  if(dpt==undefined && annee==undefined)
    var sql = "SELECT nom, SUM(quantite) as quantite FROM Nom where "+cond_sex+" nom<>\"_PRENOMS_RARES\" GROUP BY nom ORDER BY quantite DESC LIMIT 10;"
  else{
    if(dpt==undefined)
      var sql = "SELECT nom, SUM(quantite) as quantite, annee FROM Nom where "+cond_sex+" nom<>\"_PRENOMS_RARES\" AND annee="+annee+" GROUP BY nom, annee ORDER BY quantite DESC LIMIT 10;"
    else if(annee==undefined)
      var sql = "SELECT nom, SUM(quantite) as quantite, dptId FROM Nom where "+cond_sex+" nom<>\"_PRENOMS_RARES\" AND dptId="+dpt+" GROUP BY nom, dptId ORDER BY quantite DESC LIMIT 10;"
    else
      var sql = "SELECT nom, SUM(quantite) as quantite, dptId, annee FROM Nom where "+cond_sex+" nom<>\"_PRENOMS_RARES\" AND dptId="+dpt+" AND annee="+annee+" GROUP BY nom, dptId, annee ORDER BY quantite DESC LIMIT 10;";
  }

  db.query(sql, function (err, result){
    if (err) throw err;
    res.status(200).json(result);
  });
}

function getCrimes(req, res) {
  var sql = "SELECT libelle, sum(qtt) as qtt FROM Crime, CrimeDpt where Crime.crimeId=CrimeDpt.crimeId";
  if(req.query["dptId"]){
      sql += " AND CrimeDpt.dptId="+req.query.dptId;
  }
  if(req.query["crime"]){
    sql += " AND Crime.libelle=\""+req.query.crime+"\"";
  }
  sql += " group by Crime.crimeId"
  db.query(sql, function (err, result){
    if (err) throw err;
    res.status(200).json(result);
  });
}

module.exports = {
  getName: getName,
  getNames: getNames,
  getDpts: getDpts,

  getRandomName: getRandomName,
  getTop10: getTop10,

  getCrimes: getCrimes
};
