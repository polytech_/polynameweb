var config = require('./config.json')
var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var pretty = require('express-prettify');
var queries = require('./queries');

app.use(function (req, res, next) {
  res.header("Content-Type", "application/json");
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, contentType,Content-Type, Accept, Authorization");
  next();
});

app.use(pretty({ query: 'formatted' }));
app.use(bodyParser.json());

var router = express.Router();
app.use(router);
router.get('/api/v1/name', queries.getName);
router.get('/api/v1/names', queries.getNames);
router.get('/api/v1/departements', queries.getDpts);
router.get('/api/v1/random-name', queries.getRandomName);
router.get('/api/v1/top10', queries.getTop10);
router.get('/api/v1/crimes', queries.getCrimes);

app.listen(config.port, function () {
  console.log('Listening on port ' + config.port + '!');
})
app.get('*', function (req, res) {
  res.send('Express API Rest MySQL');
})
