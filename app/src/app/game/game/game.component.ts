import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
// ES6 Modules or TypeScript
import Swal from 'sweetalert2'
import { ScoreService } from '../../../app/score.service'

const _URL_name = "http://localhost:4003/api/v1/random-name";
const _URL_crime = "http://localhost:4003/api/v1/random-crime";
@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css']
})
export class GameComponent implements OnInit {
  currId: number = 0;
  isLoaded: boolean = false;
  items: any[];
  item_1: string = "";
  item_2: string = "";
  qttItem_1: number = 0;
  qttItem_2: number = 0;
  promiseGetName: any;
  scoreService: ScoreService;
  constructor(private httpClient: HttpClient, scoreService: ScoreService, private route: ActivatedRoute) 
  { this.scoreService = scoreService; 
    this.route.paramMap.subscribe( params => {
      console.log(params.get('orgstructure'));    
    });
  }


  ngOnInit() {
    this.items = [];
    this.getRandomName().then(function (response) {
      console.log("then " + JSON.stringify(response));
      this.item_1 = this.items[this.currId].nom;
      this.qttItem_1 = this.items[this.currId].quantite;
      this.currId++;
      this.item_2 = this.items[this.currId].nom;
      this.qttItem_2 = this.items[this.currId].quantite;
      this.currId++;
      this.isLoaded = true;
    }.bind(this));
    
  }


  getRandomName() {
    var that = this;
    if (this.currId + 1 > this.items.length) {
      return new Promise(function (resolve, reject) {
        that.httpClient.get<[]>(_URL_name).subscribe(res => {
          that.items = [];
          res.forEach(element => {
            that.items.push(element);
          });
          that.currId = 0;
          resolve(that.items);
        });
      });
    } else return new Promise(function (resolve, reject) {
      resolve();
    });
  }


  getRandomCrime() {
    var that = this;
    if (this.currId + 1 > this.items.length) {
      return new Promise(function (resolve, reject) {
        that.httpClient.get<[]>(_URL_crime).subscribe(res => {
          that.items = [];
          res.forEach(element => {
            that.items.push(element);
          });
          resolve(that.items);
        });
      });
    } else return new Promise(function (resolve, reject) {
      resolve();
    });
  }


  play(choice: string) {
    console.log(choice);
    if (choice == "+") {
      if (this.qttItem_2 > this.qttItem_1)
        this.nextStep();
      else
        this.loose();
    } else {
      if (this.qttItem_2 < this.qttItem_1)
        this.nextStep();
      else
        this.loose();
    }

  }

  nextStep() {
    console.log("Let's continue!");
    this.scoreService.incrScore();
    this.item_1 = this.item_2;
    this.qttItem_1 = this.qttItem_2;
    this.item_2 = "";
    this.qttItem_2 = 0;
    this.getRandomName().then(function (response) {
      do {
        this.item_2 = this.items[this.currId].nom;
        this.qttItem_2 = this.items[this.currId].quantite;
        this.currId++;  
      }while(Math.abs(this.qttItem_1-this.qttItem_2)<1000 && this.currId + 1 < this.items.length)
    }.bind(this));
  }

  loose() {
    console.log("NUL");

    Swal.fire({
      title: 'Perdu!\nScore: '+this.scoreService.getScore(),
      text: 'Voulez-vous rejouer ?',
      icon: 'error',
      confirmButtonText: 'Rejouer',
    }).then((result) => {
      this.scoreService.reset();
    })
  }



}
