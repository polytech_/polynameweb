import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { Label } from 'ng2-charts';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  formName: FormGroup;
  formTop10: FormGroup;
  formCrimes: FormGroup;
  isLoadedName: boolean = false;
  isLoadedCrime: boolean = false;
  error: string = "Loading ...";
  numberForName: string = "";
  names: any[];
  crimes: any[];
  crimesChart: boolean = true;
  crimeStat: any;
  departements: any[];
  annees: any[];
  downloadJsonHref: any;
  downloadJsonHrefSchema: any;

  constructor(
    private fb: FormBuilder,
    private httpClient: HttpClient,
    private sanitizer: DomSanitizer
  ) { }

  public namesLabels: Label[] = [];
  public namesData: ChartDataSets[] = [];
  public namesOptions: ChartOptions = { responsive: true };
  public namesLegend: boolean = false;
  public namesChartType: ChartType = 'horizontalBar';
  public namesColor: any[] = [{ backgroundColor: '#00B7D6' }];

  public crimesChartType: string = 'doughnut';
  public crimesDatasets: Array<any> = [];
  public crimesLabels: Array<any> = [];
  public crimesOptions: any = { responsive: true };
  public crimesChartClicked(e: any): void { }
  public crimesChartHovered(e: any): void { }

  ngOnInit() {
    this.names = [];
    this.crimes = [];
    this.departements = [];
    this.annees = [];

    this.formName = this.fb.group({
      nom: this.fb.control(undefined),
      departement: this.fb.control(undefined),
      annee: this.fb.control(undefined),
    });

    this.formTop10 = this.fb.group({
      sexe: this.fb.control(undefined),
      departement: this.fb.control(undefined),
      annee: this.fb.control(undefined),
    });

    this.formCrimes = this.fb.group({
      departement: this.fb.control(undefined),
      crime: this.fb.control(undefined)
    });

    for (let i = 1900; i < 2019; i++) {
      this.annees.push(i);
    }
    this.getDpts();

    this.onChangesTop10();
    this.onChangesCrimes();
  }

  onChangesTop10(): void {
    this.formTop10.valueChanges.subscribe(form => {
      let options = {};
      if (form["sexe"] != undefined && form["sexe"] != "") options["sexe"] = form["sexe"];
      if (form["departement"] != undefined && form["departement"] != "") options["dptId"] = parseInt((form["departement"].split(" "))[0]);
      if (form["annee"] != undefined && form["annee"] != "") options["annee"] = form["annee"];
      this.getTop10(options);
    });
  }

  onChangesCrimes(): void {
    this.formCrimes.valueChanges.subscribe(form => {
      let options = {};
      if (form["departement"] != undefined && form["departement"] != "") options["dptId"] = parseInt((form["departement"].split(" "))[0]);
      if (form["crime"] != undefined && form["crime"] != "") options["crime"]=form["crime"];
      this.getCrimes(options);
    });
  }

  getDpts() {
    this.httpClient.get<[]>(_URL_dpt)
      .subscribe(
        res => {
          res.forEach(element => {
            this.departements.push(element);
          });
          this.getTop10({});
          this.getCrimes({});
        },
        error => { console.log("Connexion serveur impossible");}
      );
  }

  getTop10(options) {
    this.httpClient.get<[]>(_URL_top10, { params: options }).subscribe(res => {
      this.names = [];
      let labels = [];
      let dataNames = [];
      res.forEach(element => {
        this.names.push(element);
      });

      this.names.forEach(element => {
        labels.push(element["nom"]);
        dataNames.push(element["quantite"]);
      });

      this.namesLabels = labels;
      this.namesData = [{ data: dataNames, label: '' }];

      this.generateDownloadJsonUri();
      this.generateDownloadSchemaUri();
      this.isLoadedName=true;
    });
  }

  getCrimes(options) {
    this.httpClient.get<[]>(_URL_crimes, { params: options }).subscribe(res => {
      let crimes2 = [];
      let size = 0;
      res.forEach(element => {
        crimes2.push(element);
        size++;
      })
      if(size==1){
        this.crimesChart = false;
        res.forEach(element => {
          this.crimeStat=element;
        });
        console.log(this.crimes)
      }
      else{
        this.crimesChart = true;
        this.crimes = crimes2;
        console.log(this.crimes)
        let labels = [];
        let dataCrimes = [];
        this.crimes.forEach(element => {
          labels.push(element["libelle"]);
          dataCrimes.push(element["qtt"]);
        });
        this.crimesLabels = labels;
        this.crimesDatasets = [{ data: dataCrimes, label: ''}];
        // console.log(this.crimesDatasets)

      }
      this.isLoadedCrime=true;
      // this.generateDownloadJsonUri();
    });
  }

  generateDownloadJsonUri() {
    let json = {
      options: {},
      data: []
    };
    json.options = this.formTop10.value;
    json.data = this.names;
    var uri = this.sanitizer.bypassSecurityTrustUrl("data:text/json;charset=UTF-8," + encodeURIComponent(JSON.stringify(json)));
    this.downloadJsonHref = uri;
  }

  generateDownloadSchemaUri() {
    this.downloadJsonHrefSchema = 'assets/schema.json';
  }

  searchName(){
    let options = {};
    this.numberForName="0"
    if (this.formName.value["nom"] != undefined && this.formName.value["nom"] != ""){
      options["nom"] = this.formName.value["nom"].toUpperCase();
      if (this.formName.value["departement"] != undefined && this.formName.value["departement"] != "") options["dptId"] = parseInt((this.formName.value["departement"].split(" "))[0]);
      if (this.formName.value["annee"] != undefined && this.formName.value["annee"] != "") options["annee"] = this.formName.value["annee"];
      this.httpClient.get<[]>(_URL_searchName, { params: options }).subscribe(res => {
        res.forEach(element => {
          this.numberForName = element["quantite"];
        });
      });
    }
  }
}

const _URL_searchName = "http://localhost:4003/api/v1/name";
const _URL_top10 = "http://localhost:4003/api/v1/top10";
const _URL_dpt = "http://localhost:4003/api/v1/departements";
const _URL_crimes = "http://localhost:4003/api/v1/crimes";

const ParseHeaders = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};
