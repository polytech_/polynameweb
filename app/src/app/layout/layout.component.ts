import { Component, OnInit, Input } from '@angular/core';
import { ScoreService } from '../score.service';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit {

  score: number;
  scoreService: ScoreService;
  constructor(scoreService: ScoreService) {
    this.scoreService = scoreService;
  }

  ngOnInit() {
  }

}
