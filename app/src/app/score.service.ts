import { Injectable } from '@angular/core';
import { LayoutComponent } from '../app/layout/layout.component';
import { Observable, of } from 'rxjs';

@Injectable()
export class ScoreService {
  reset() {
    this.score = 0;
  }

  score:number = 0;

  constructor() {
  }

  getScore()
  {
    return this.score;
  }

  incrScore()
  {
    this.score++;
    console.log("ici"+this.score);
  }
}
