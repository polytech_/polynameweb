import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.component.html',
  styleUrls: ['./accueil.component.css']
})
export class AccueilComponent implements OnInit {

  constructor(private rooter : Router) { }

  ngOnInit() {
  }

  goTo(index:String){
    console.log("goTo => "+index);
    this.rooter.navigate(['/game'], {queryParams: {orgstructure: 1}});
    //this.rooter.navigateByUrl('/game?id=33');

  }

}
